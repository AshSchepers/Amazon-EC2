
package json;

import static java.util.stream.Collectors.*;
import static json.JsonStream.*;
import static org.junit.Assert.*;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.junit.BeforeClass;
import org.junit.Test;

public class JsonStreamTest {
    
	@Test
	public void test01() throws IOException {
        String s = "{\"floats\" : [1.15e+4, 2E-1, -3e2, 6.1]}";
        decode(s)
            .asObject()
            .stream()
            .map(p -> p.getKey() + " -> " + p.getValue())
            .forEach(p -> assertEquals("floats -> [11500.0,0.2,-300.0,6.1]", p));
    }

    @Test
    public void test02() throws IOException {
        String s = "{\"hi\":1, \"hey\":{\"a\":2,\"b\":[true,false,null]}, \"bi\":[1,2,3]}";

        String result = 
            decode(s)
            .asObject()
            .stream()
            .filter(p -> {
                String k = p.getKey();
                return k.equals("hi") || k.equals("bi");
            })
            .map(p -> p.getKey() + ":" + p.getValue())
            .collect(joining(";"));

        assertEquals(result, "hi:1;bi:[1,2,3]");
    }
}
