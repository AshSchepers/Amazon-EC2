
package json;

import static json.JsonStream.*;
import static org.junit.Assert.*;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.junit.BeforeClass;
import org.junit.Test;

import json.Json;

public class JsonTest {

	@Test
	public void test01() throws IOException {
        double[] vals = {1.15e+4, 2E-1, -3e2, 6.1};
        String s = "{\"floats\" : [1.15e+4, 2E-1, -3e2, 6.1]}";
        Json json = Json.decode(s);

        List<Json> floats = json.get("floats").asArray();

        int i = 0;
        for (Json t : floats) {
            assertEquals(t.toString(), String.valueOf(vals[i++]));
        }
    }

    @Test
    public void test02() throws IOException {
        String s = "{\"hi\":1, \"hey\":{\"a\":2,\"b\":[true,false,null]}, \"bi\":[1,2,3]}";

        Json j = Json.fromObjectStream(decode(s)
                .asObject()
                .stream()
                .filter(p -> {
                    String k = p.getKey();
                    return k.equals("hi") || k.equals("hey");
                }));

        assertNotNull(j);
        
        Json hi = j.get("hi");
        assertNotNull(hi);
        assertTrue(hi.isInt());
        assertEquals(hi.asInt(), 1);

        Json hey = j.get("hey");
        assertNotNull(hey);
        assertTrue(hey.isObject());
        assertEquals(hey.get("a").asInt(), 2);
        assertEquals(hey.get("b").toString(), "[true,false,null]");
    }
}
