import pytest
import unittest
import src.website.index as ind 

@pytest.fixture
def client():
    #ind.main()
    return ind.app.test_client()
    print("ended")

def test_load_user(client):
    #not sure what is needed here
    pass

def test_get_form_vals_instances(client):
    #can give data not sure best way to test if it is correct and might be used later in another fucntion
    pass
def test_non_user_redirect(client):
        #test all the pages that you get redirected if not logged in due to logging out not working
        assert client.get('/dashboard').status_code == 302

def test_index(client):
    #havent tested with the selcted and when logged in
    assert client.get('/').status_code ==200
    assert client.get('/index').status_code == 200




def test_register(client):
    #testing showed that sesion username stays even after logout so register before testing login-logout

    #test if pages loads
    resp = client.get('/register')
    assert 'Amazon Registration' in resp.data
    #not going to test regex that is expected to work
    resp = client.post('/register', data=dict(username='r',firstname='regname', lastname='regsurname', email='register@test.com', password='Reguser.1'),follow_redirects=True)
    assert 'Your username is not of the correct format.' in resp.data
    resp = client.post('/register', data=dict(username='reguser',firstname='', lastname='regsurname', email='register@test.com', password='Reguser.1'),follow_redirects=True)
    assert 'Your firstname must be at least one character.' in resp.data
    resp = client.post('/register', data=dict(username='reguser',firstname='regname', lastname='', email='register@test.com', password='Reguser.1'),follow_redirects=True)
    assert 'Your lastname must be at least one character.' in resp.data
    resp = client.post('/register', data=dict(username='reguser',firstname='regname', lastname='regsurname', email='re', password='Reguser.1'),follow_redirects=True)
    assert 'Incorrect email format.' in resp.data
    resp = client.post('/register', data=dict(username='reguser',firstname='regname', lastname='regsurname', email='register@test.com', password='Reguse'),follow_redirects=True)
    assert 'Password invalid!' in resp.data
    resp = client.post('/register', data=dict(username='tester',firstname='regname', lastname='regsurname', email='register@test.com', password='Reguser.1'),follow_redirects=True)
    assert 'Username already exists' in resp.data
    resp = client.post('/register', data=dict(username='reguser',firstname='regname', lastname='regsurname', email='test@test.com', password='Reguser.1'),follow_redirects=True)
    assert 'Email already taken!' in resp.data
    #wont run it since there isnt a method to delete a registered user
    #resp = client.post('/register', data=dict(username='reguser',firstname='regname', lastname='regsurname', email='register@test.com', password='Reguser.1'),follow_redirects=True)
    #assert resp.satus_code == 304 # gets redirected if correct


    #test redirected to index if logged in
    resp =client.post('/login', data=dict(username='tester', password='Tester.1'), follow_redirects=True)
    resp = client.get('/register')
    assert "index" in resp.data
    client.get('/logout')



# username tester password Tester.1
def test_login_logout(client):
    #didnt test verified user data
    assert client.get('/login').status_code == 200
    resp = client.post('/login', data=dict(username='', password='Tester.1'), follow_redirects=False)
    assert 'Please enter your credentials!' in resp.data

    resp =  client.post('/login', data=dict(username ='tester', password='wrong'),follow_redirects=True)
    assert 'Invalid login credentials!' in resp.data
    resp = client.post('/login', data=dict(username='tester',password='Tester.1'),follow_redirects=True)
    #assert 'table-instances' in resp.data
    #logout
    assert 'index' in client.get('/logout').data #redirected to index page
    assert 'login' in client.get('/logout').data #redirect to login if not signed in


def test_dashboard(client):
    #not logged in redirect done earlier
    resp = client.post('/login', data=dict(username='tester',password='Tester.1'),follow_redirects=True)
    resp = client.get('/dashboard')
    assert 'Account Dashboard' in resp.data
   
def test_update_info(client):
    resp = client.post('login', data=dict(username='tester',password='Tester.1'),follow_redirects=True)
    #cant visit page gets redirected
    assert client.get('/update_info').status_code == 302
    resp = client.post('/update_info',data=dict(firstname='nameupdate', lastname='tester',email='test@test.com'),follow_redirects=True)
    #check if new firstname is in dashboard
    assert 'nameupdate' in client.get('/dashboard').data
    resp = client.post('/update_info', data=dict(firstname='testing', lastname='tester',email='test@test.com'),follow_redirects=True)
    resp = client.post('/update_info', data=dict(payment = 'eu-west-1'),follow_redirects=True)
    #check that new price is selected
    #not working now since all paymetn regions got deleted
    assert 'eu-west-1' in resp.data
    resp = client.post('/update_info', data=dict(payment='ap-northeast-1'),follow_redirects=True)


def test_bookmarking(client):
    resp = client.post('/login', data=dict(username='tester',password='Tester.1'),follow_redirects=True)
    
    resp = client.post('/bookmark',data=dict(bookmark='c3.4xlarge'),follow_redirects=True)
    assert 'c3.4xlarge' in client.get('/bookmarked').data
    resp =   client.get('/index')
    resp = client.post('/unbookmark',data=dict(unbookmark='c3.4xlarge'),follow_redirects=True)
    assert client.get('/index').status_code ==200
    assert 'c3.4xlarge' not in client.get('/bookmarked').data

def test_change_password(client):
    resp = client.post('/login', data=dict(username='tester',password='Tester.1'), follow_redirects=True)
    resp = client.post('/change_password', data=dict(oldpassword='test',newpassword='test'), follow_redirects=True)
    assert 'Old password incorrect!' in resp.data
    resp = client.post('/change_password', data=dict(oldpassword='Tester.1', newpassword='hi'))
    assert 'New password invalid!' in resp.data
    resp = client.post('/change_password', data=dict(oldpassword='Tester.1', newpassword='Tester.2'), follow_redirects=True)
    assert resp.status_code == 200
    resp = client.post('/change_password', data=dict(oldpassword='Tester.2', newpassword='Tester.1'), follow_redirects=True)
    assert resp.status_code == 200
    

def test_admin_login(client):
    assert client.get('/adm/admin_login').status_code ==302

def test_add_instance(client):
    assert client.get('/add_instance').status_code == 302

def test_change_instance(client):
    assert client.get('/change_instance/c1.mdeium').status_code == 302

def test_compare(client):
    resp = client.post('/login', data=dict(username='tester', password='Tester.1'), follow_redirects=True)
    resp = client.get('/select/c1.xlarge')
    resp = client.get('/compare_selected')
    assert 'c1.xlarge' in resp.data
    resp = client.get('/select/c1.xlarge')
    resp = client.get('compare_selected')
    assert 'c1.xlarge' not in resp.data


