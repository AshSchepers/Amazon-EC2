import sys
import pytest
import json
import src.website.functions as func
import src.website.index

def test_get_prices():
    prices = func.get_prices() 
    print ("Asserting all price regions returned")
    # just showing that atleast they are regions
    assert   "eu-west-1" in prices
    assert 'ap-northeast-1' in prices
    assert 'sa-east-1' in prices
    assert 'ap-southeast-1' in prices
    assert 'ap-southeast-2' in prices
    assert 'us-west-2' in prices
    assert 'us-west-1' in prices
    
def test_get_unique():
    print ("Asserting unique sorts correctly")
    assert  func.get_unique([4,4,1,2]) == [1,2,4]

def test_get_vals_of():
    print ("Asserting correct values ")
    assert  func.get_vals_of('vCPUs') == [1,2,4,8,12,16,24,32,36,40,48,64,72,96,128]   
    
def test_get_vals_of_list():
    print ("Asserting correct list values ")
    assert  func.get_vals_of_list('features') == ['iops','piops']  
    
def test_get_vals_of_object():
    print ("Asserting correct values of object returned")
    assert  func.get_vals_of_object('cpuOptions', 'threadsPerCore') == [1,2]     
    
def test_get_instances():
    instances = func.get_instances() 
    errors = []
    print ("Asserting all instances returned")
    assert  len(instances) > 1 #not just one     
    #only tested the values in the index page and not pricing and bookmarks     
    with open('src/database/ec2instances-valid.json') as f:
        json_instances = json.load(f)
        for json_instance in json_instances:
            found=False
            for inst in instances:
                if inst['name'] == json_instance:
                    found = True
    assert not errors,"errors occured:\n{}".format("\n".join(errors))  

def test_get_instance():
	instances = func.get_instances()
	setter ={"name": "aa.smallest", "_type": "ec2.instancetypes.InstanceType", "vCPUs": "350", "memoryKB": "1740", "features": instances[0]['features'], "virtualizations":['hvm', 'paravirtual'], "architectures": ['i386', 'x86_64'], "placementGroupsSupported": instances[0]['placementGroupsSupported'], "rootDeviceTypes": ['legacyStore','instanceStore','ebsStore'], "ephemeralsSupported": instances[0]['ephemeralsSupported'], "publicFacing": instances[0]['publicFacing'], "dedicatedTenancySupported": instances[0]['dedicatedTenancySupported'], "ipv6Supported": instances[0]['ipv6Supported'], "egpuSupported": instances[0]['egpuSupported'], "enaRequired": instances[0]['publicFacing'], "platforms": ['vpc', 'ec2'], "hypervisors": ['xen','xen4','ovm'], "cpuOptions": instances[0]['cpuOptions']}
	func.insert_instance(setter)
	resp = func.get_instance('aa.smallest')
	func.delete_instance('aa.smallest')
	assert resp['Item'] == setter

def test_insert_instance():
	instances = func.get_instances()
	setter ={"name": "aa.smallest", "_type": "ec2.instancetypes.InstanceType", "vCPUs": "350", "memoryKB": "1740", "features": instances[0]['features'], "virtualizations":['hvm', 'paravirtual'], "architectures": ['i386', 'x86_64'], "placementGroupsSupported": instances[0]['placementGroupsSupported'], "rootDeviceTypes": ['legacyStore','instanceStore','ebsStore'], "ephemeralsSupported": instances[0]['ephemeralsSupported'], "publicFacing": instances[0]['publicFacing'], "dedicatedTenancySupported": instances[0]['dedicatedTenancySupported'], "ipv6Supported": instances[0]['ipv6Supported'], "egpuSupported": instances[0]['egpuSupported'], "enaRequired": instances[0]['publicFacing'], "platforms": ['vpc', 'ec2'], "hypervisors": ['xen','xen4','ovm'], "cpuOptions": instances[0]['cpuOptions']}
	func.insert_instance(setter)
	resp = func.get_instance('aa.smallest')
	func.delete_instance('aa.smallest')
	assert resp['Item'] == setter

def test_update_instance():
	instances = func.get_instances()
	setter ={"name": "aa.smallest", "_type": "ec2.instancetypes.InstanceType", "vCPUs": "350", "memoryKB": "1740", "features": instances[0]['features'], "virtualizations":['hvm', 'paravirtual'], "architectures": ['i386', 'x86_64'], "placementGroupsSupported": instances[0]['placementGroupsSupported'], "rootDeviceTypes": ['legacyStore','instanceStore','ebsStore'], "ephemeralsSupported": instances[0]['ephemeralsSupported'], "publicFacing": instances[0]['publicFacing'], "dedicatedTenancySupported": instances[0]['dedicatedTenancySupported'], "ipv6Supported": instances[0]['ipv6Supported'], "egpuSupported": instances[0]['egpuSupported'], "enaRequired": instances[0]['publicFacing'], "platforms": ['vpc', 'ec2'], "hypervisors": ['xen','xen4','ovm'], "cpuOptions": instances[0]['cpuOptions']}
	func.insert_instance(setter)
	setter['vCPUs'] = 300
	func.update_instance("aa.smallest",setter)
	resp = func.get_instance('aa.smallest')
	func.delete_instance('aa.smallest')
	assert resp['Item'] == setter


def test_delete_instance():
    instances = func.get_instances()
    setter ={"name": "aa.smallest", "_type": "ec2.instancetypes.InstanceType", "vCPUs": "350", "memoryKB": "1740", "features": instances[0]['features'], "virtualizations":['hvm', 'paravirtual'], "architectures": ['i386', 'x86_64'], "placementGroupsSupported": instances[0]['placementGroupsSupported'], "rootDeviceTypes": ['legacyStore','instanceStore','ebsStore'], "ephemeralsSupported": instances[0]['ephemeralsSupported'], "publicFacing": instances[0]['publicFacing'], "dedicatedTenancySupported": instances[0]['dedicatedTenancySupported'], "ipv6Supported": instances[0]['ipv6Supported'], "egpuSupported": instances[0]['egpuSupported'], "enaRequired": instances[0]['publicFacing'], "platforms": ['vpc', 'ec2'], "hypervisors": ['xen','xen4','ovm'], "cpuOptions": instances[0]['cpuOptions']}

    func.insert_instance(setter)
    resp = func.get_instance('aa.smallest') 
    func.delete_instance('aa.smallest')
    assert len(instances) == len(func.get_instances())
