
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

//import org.apache.commons.exec.CommandLine;
//import org.apache.commons.exec.DefaultExecutor;
//import org.apache.commons.exec.PumpStreamHandler;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import json.JsonStreamTest;
import json.JsonTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    JsonStreamTest.class,
    JsonTest.class
})

public class Tests {

	static {
		InputStream is = Tests.class.getClassLoader()
				.getResourceAsStream("build.properties");
		if (is != null) {
			Properties p = new Properties();
			try {
				p.load(is);
			} catch (IOException e) {
				// do nothing
			}
		}
	}
}
