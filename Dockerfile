FROM openjdk:8

MAINTAINER Ashleigh Schepers <ashleighschepers37@gmail.com>

 

# Update the system
RUN apt update -y
RUN apt upgrade -y
RUN apt-get install -y python
RUN apt-get install -y python-pip

# Install relevant packages
RUN apt install ant -y
RUN apt install vim -y

# Clone down the Amazon repository
WORKDIR Amazon-EC2
ADD . /Amazon-EC2/

