
package json;

import java.io.File;
import java.io.IOException;
import java.io.Reader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

/**
 * Class for eager evaluation of json data. Json is just an algebraic type
 * which can be constructed recursively from a JsonStream.Value object. 
 */
public final class Json {

    /**
     * Decodes a stream of json text from a reader.
     * @param Reader reader a reader object which provides the character stream
     * from some source
     * @return Json the parsed json value
     */
    public static Json decode(Reader reader) throws IOException {
        return new Json(JsonStream.decode(reader));
    }

    /**
     * Decodes a json string.
     * @param String s the json text
     * @return Json the parsed json value
     */
    public static Json decode(String s) throws IOException {
        return new Json(JsonStream.decode(s));
    }

    /**
     * Decodes a json string from a File.
     * @param File f the file containing the json text
     * @return Json the parsed json value
     */
    public static Json decode(File f) throws IOException {
        return new Json(JsonStream.decode(f));
    }

    /// type
    private final Type type;

    /// concrete values
    private int intVal;
    private double floatVal;
    private boolean boolVal;
    private String stringVal;
    private List<Json> arrayVal;
    private HashMap<String, Json> objVal;

    /**
     * Construct a Json value recursively from a stream of Values
     * @param Stream<JsonStream.Value> vals the values which the array will contain
     * @return The constructed Json object
     */
    public static Json fromArrayStream(Stream<JsonStream.Value> vals) {
        final Json j = new Json(Type.ARRAY);

        vals.forEach(x -> {
            j.arrayVal.add(new Json(x));
        });

        return j;
    }

    /**
     * Construct a Json value recursively from a stream of pairs
     * @param Stream<Pair<String,JsonStream.Value>> pairs the pairs which the object
     * will contain
     * @return The constructed Json object
     */
    public static Json fromObjectStream(Stream<Pair<String, JsonStream.Value>> pairs) {
        final Json j = new Json(Type.OBJECT);

        pairs.forEach(p -> {
            j.objVal.put(p.getKey(), new Json(p.getValue()));
        });

        return j;
    }

    /**
     * Construct a Json value recursively from a lazy JsonStream.Value object
     * @param JsonStream.Value v the lazy value from some input stream
     */
    public Json(JsonStream.Value v) {
        this(v.getType());
        switch (type) {
            case NULL:
                break;
            case INT:
                intVal = v.asInt();
                break;
            case FLOAT:
                floatVal = v.asFloat();
                break;
            case BOOLEAN:
                boolVal = v.asBoolean();
                break;
            case STRING:
                stringVal = v.asString();
                break;

            case ARRAY:
                // recursive motherfucker
                v.asArray().forEachRemaining(x -> {
                    arrayVal.add(new Json(x));
                });
                break;

            case OBJECT:
                // recursive motherfucker
                v.asObject().forEachRemaining(p -> {
                    objVal.put(p.getKey(), new Json(p.getValue()));
                });
                break;
        }
    }

    /**
     * Construct a Json value recursively from a lazy JsonStream.Array object
     * @param JsonStream.Array arr the lazy value from some input stream
     */
    public Json(JsonStream.Array arr) {
        this(Type.ARRAY);
        arr.forEachRemaining(x -> {
            arrayVal.add(new Json(x));
        });
    }

    /**
     * Construct a Json value recursively from a lazy JsonStream.Object
     * @param JsonStream.Object obj the lazy value from some input stream
     */
    public Json(JsonStream.Object obj) {
        this(Type.OBJECT);
        obj.forEachRemaining(p -> {
            objVal.put(p.getKey(), new Json(p.getValue()));
        });
    }

    /**
     * Constructs a Json value with type = type
     * @param Type type the type of the json value
     */
    public Json(Type type) {
        this.type = type;
        switch (type) {
            case ARRAY:
                arrayVal = new ArrayList<>();
                break;
            case OBJECT:
                objVal = new HashMap<>();
        }
    }

    /**
     * Constructs a null object
     */
    public Json() {
        type = Type.NULL;
    }

    /**
     * Constructs a json int with value i
     * @param int i the value
     */
    public Json(int i) {
        type = Type.INT;
        intVal = i;
    }

    /**
     * Constructs a json float with value d
     * @param double d the value
     */
    public Json(double d) {
        type = Type.FLOAT;
        floatVal = d;
    }

    /**
     * Constructs a json string with value s
     * @param String s the value
     */
    public Json(String s) {
        type = Type.STRING;
        stringVal = s;
    }

    /**
     * Constructs a json boolean with value b
     * @param boolean b the value
     */
    public Json(boolean b) {
        type = Type.BOOLEAN;
        boolVal = b;
    }

    /**
     * Constructs a json array
     * The array is not copied
     * @param List<Json> arr the values
     */
    public Json(List<Json> arr) {
        type = Type.ARRAY;
        arrayVal = arr;
    }

    /**
     * Constructs a json array
     * @param Json... arr the values
     */
    public Json(Json... arr) {
        type = Type.ARRAY;
        arrayVal = Arrays.asList(arr);
    }

    /**
     * Constructs a json object
     * The map is not copied
     * @param Hashmap<String,Json> map the json object
     */
    public Json(HashMap<String, Json> map) {
        type = Type.OBJECT;
        objVal = map;
    }

    /**
     * Gets the type
     * @return Type type
     */
    public Type getType() {
        return type;
    }

    /**
     * Queiries if the type is null
     * @return true if is null
     */
    public boolean isNull() {
        return type == Type.NULL;
    }

    /**
     * Queiries if the type is int
     * @return true if is int
     */
    public boolean isInt() {
        return type == Type.INT;
    }

    /**
     * Queiries if the type is float
     * @return true if is float
     */
    public boolean isFloat() {
        return type == Type.FLOAT;
    }

    /**
     * Queiries if the type is boolean
     * @return true if is boolean
     */
    public boolean isBoolean() {
        return type == Type.BOOLEAN;
    }

    /**
     * Queiries if the type is string
     * @return true if is string
     */
    public boolean isString() {
        return type == Type.STRING;
    }

    /**
     * Queiries if the type is array
     * @return true if is array
     */
    public boolean isArray() {
        return type == Type.ARRAY;
    }

    /**
     * Queiries if the type is object
     * @return true if is object
     */
    public boolean isObject() {
        return type == Type.OBJECT;
    }

    /**
     * Converts to int.
     * Throws a JsonException if the type is not convertable
     * @return int value
     */
    public int asInt() {
        if (!isInt()) {
            throw new JsonException("Incorrect Type");
        }

        return intVal;
    }

    /**
     * Converts to float.
     * Throws a JsonException if the type is not convertable
     * @return double value
     */
    public double asFloat() {
        if (!isFloat()) {
            throw new JsonException("Incorrect Type");
        }

        return floatVal;
    }

    /**
     * Converts to boolean.
     * Throws a JsonException if the type is not convertable
     * @return boolean value
     */
    public boolean asBoolean() {
        if (!isBoolean()) {
            throw new JsonException("Incorrect Type");
        }

        return boolVal;
    }

    /**
     * Converts to string.
     * Throws a JsonException if the type is not convertable
     * @return String value
     */
    public String asString() {
        if (!isString()) {
            throw new JsonException("Incorrect Type");
        }

        return stringVal;
    }

    /**
     * Converts to array.
     * Throws a JsonException if the type is not convertable
     * @return List<Json> value
     */
    public List<Json> asArray() {
        if (!isArray()) {
            throw new JsonException("Incorrect Type");
        }

        return arrayVal;
    }

    /**
     * Converts to object.
     * Throws a JsonException if the type is not convertable
     * @return HashMap<String,Json> value
     */
    public HashMap<String, Json> asObject() {
        if (!isObject()) {
            throw new JsonException("Incorrect Type");
        }

        return objVal;
    }

    /**
     * Indexes the hashtable at the specified index.
     * @param key the key to get the value from
     * @return the Json value at the given index
     */
    public Json get(String key) {
        return asObject().get(key);
    }

    /**
     * Indexes the array at the specified index.
     * @param i the index to get the value from
     * @return the Json value at the given index
     */
    public Json get(int i) {
        return asArray().get(i);
    }

    @Override
    public String toString() {
        switch (type) {
            case NULL:
                return "null";
            case INT:
                return String.valueOf(intVal);
            case FLOAT:
                return String.valueOf(floatVal);
            case BOOLEAN:
                return String.valueOf(boolVal);
            case STRING:
                // TODO quotes and escapes
                return stringVal;
            case ARRAY:
                {
                    StringBuilder sb = new StringBuilder();
                    sb.append('[');
                    boolean first = true;
                    for (Json j : arrayVal) {
                        if (!first) {
                            sb.append(',');
                        }
                        first = false;
                        sb.append(j.toString());
                    }
                    sb.append(']');
                    return sb.toString();
                }
            case OBJECT:
                {
                    StringBuilder sb = new StringBuilder();
                    sb.append('{');
                    boolean first = true;
                    for (HashMap.Entry<String,Json> e : objVal.entrySet()) {
                        if (!first) {
                            sb.append(',');
                        }
                        first = false;
                        sb.append(e.getKey());
                        sb.append(':');
                        sb.append(e.getValue().toString());
                    }
                    sb.append('}');
                    return sb.toString();
                }
        }
        throw new RuntimeException("Unreachable code");
    }
}

