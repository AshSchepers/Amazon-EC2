/**
 * Scott Cameron was extremely useful in ideas and various implementations. He
 * suggested the usage of a functional approach.
 */

package json;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import java.lang.StringBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Lazy json parsing api. Parses the json such that Arrays/Objects are
 * iterators.
 * The Json class provides a representation for storing json data in HashMaps
 * and Lists. 
 * Arrays and Objects set their type to null after conversion.
 *
 * Since the text is sourced from a Reader object, and with the stream design,
 * any json Value obtained from an Array or Object must be processed before the
 * next Value can be requested from that Array or Object. 
 * */
public final class JsonStream {

    /**
     * Decodes a stream of json text from a reader. A Value is computed from the
     * input stream of characters; if the text represents a simple value, it is
     * just consumed up until the end of the value, otherwise if the text
     * represents an array or object, only the opening character of the
     * array/object is read and the value is returned immediately to be
     * processed lazily. 
     * @param Reader reader a reader object which provides the character stream
     * from some source
     * @return Value the parsed json value
     */
    public static Value decode(Reader reader) throws IOException {
        return new Value(new CharStream(reader));
    }

    /**
     * Decodes a json string.
     * @see public static Value decode(Reader reader);
     * @param String s the json text
     * @return Value the parsed json value
     */
    public static Value decode(String s) throws IOException {
        return decode(new StringReader(s));
    }

    /**
     * Decodes a json string from a File.
     * @see public static Value decode(Reader reader);
     * @param File f the file containing the json text
     * @return Value the parsed json value
     */
    public static Value decode(File f) throws IOException {
        return decode(new BufferedReader(new FileReader(f)));
    }

    /**
     * Class to encapsulate a Reader with one lookahead character.
     * This allows Values to share the same Reader and lookahead
     */
    private static final class CharStream {

        private Reader reader;
        private int c;

        /**
         * Assigns a Reader and the initial value for the
         * lookahead character
         */
        private CharStream(Reader reader, int c) {
            this.reader = reader;
            this.c = c;
        }

        /**
         * Assigns a Reader and reads in the first
         * character from the stream
         */
        private CharStream(Reader reader) {
            this.reader = reader;
            forceSkipWhite();
        }

        /**
         * Gets the current lookahead char
         * @return the next char
         */
        public int getC() {
            return c;
        }

        /**
         * Reads the next char
         * @return the next char
         */
        public int next() {
            try {
                c = reader.read();
            } catch (IOException e) {
                e.printStackTrace(System.err);
                c = -1;
            }
            return c;
        }

        /**
         * Like next() but throws a JsonException if the stream is empty
         * @return the next char
         */
        public int forceNext() {
            if (next() < 0) {
                throw new JsonException("Reached end of input while decoding");
            }

            return c;
        }

        /**
         * Reads the next char while skipping white space
         * @return the next non-white char
         */
        public int skipWhite() {
            do {
                next();
            } while (c >= 0 && Character.isWhitespace(c));

            return c;
        }

        /**
         * Like skipWhite() but throws a JsonException if the stream is empty
         * @return the next non-white char
         */
        public int forceSkipWhite() {
            if (skipWhite() < 0) {
                throw new JsonException("Reached end of input while decoding");
            }

            return c;
        }
    }

    /**
     * Represents a json value. A Value can be an int, double, boolean, string,
     * array, object or null. The simple types, int, double, boolean, string 
     * and null, are parsed eagerly for immediate use, but compound types,
     * array and object are parsed lazily to reduce memory usage and computation
     * time. This means that the values of simple types can be reused while the
     * values of compound types cannot. In particular after requesting the value
     * of a compound type with val.asObject() or val.asArray(), the Value object
     * changes type to Type.NULL so that it cannot be reused.
     */
    public static final class Value {

        /// input stream
        private final CharStream chars;
        /// used to enforce inorder traversal
        private boolean doneReading;
        private Type type;

        /// concrete values
        private int intVal;
        private double floatVal;
        private boolean boolVal;
        private String stringVal;

        /// reference to lazy objects
        private Array arr;
        private Object obj;

        /**
         * Constructs and parses a Value from a character stream
         */
        private Value(CharStream chars) {
            this.chars = chars;
            type = Type.NULL;
            readValue();
        }

        /**
         * Gets the type of this Value
         * @return Type type
         */
        public Type getType() {
            return type;
        }

        /**
         * Queiries if the type is null
         * @return true if is null
         */
        public boolean isNull() {
            return type == Type.NULL;
        }

        /**
         * Queiries if the type is int
         * @return true if is int
         */
        public boolean isInt() {
            return type == Type.INT;
        }

        /**
         * Queiries if the type is float
         * @return true if is float
         */
        public boolean isFloat() {
            return type == Type.FLOAT;
        }

        /**
         * Queiries if the type is boolean
         * @return true if is boolean
         */
        public boolean isBoolean() {
            return type == Type.BOOLEAN;
        }

        /**
         * Queiries if the type is string
         * @return true if is string
         */
        public boolean isString() {
            return type == Type.STRING;
        }

        /**
         * Queiries if the type is array
         * @return true if is array
         */
        public boolean isArray() {
            return type == Type.ARRAY;
        }

        /**
         * Queiries if the type is object
         * @return true if is object
         */
        public boolean isObject() {
            return type == Type.OBJECT;
        }

        /**
         * Converts to int.
         * Throws a JsonException if the type is not convertable
         * @return int value
         */
        public int asInt() {
            if (!isInt()) {
                throw new JsonException("Incorrect Type");
            }

            return intVal;
        }

        /**
         * Converts to float.
         * Throws a JsonException if the type is not convertable
         * @return double value
         */
        public double asFloat() {
            if (!isFloat()) {
                throw new JsonException("Incorrect Type");
            }

            return floatVal;
        }

        /**
         * Converts to boolean.
         * Throws a JsonException if the type is not convertable
         * @return boolean value
         */
        public boolean asBoolean() {
            if (!isBoolean()) {
                throw new JsonException("Incorrect Type");
            }

            return boolVal;
        }

        /**
         * Converts to string.
         * Throws a JsonException if the type is not convertable
         * @return String value
         */
        public String asString() {
            if (!isString()) {
                throw new JsonException("Incorrect Type");
            }

            return stringVal;
        }

        /**
         * Converts to array.
         * Throws a JsonException if the type is not convertable
         * @return Array value
         */
        public Array asArray() {
            if (!isArray()) {
                throw new JsonException("Incorrect Type");
            }

            type = Type.NULL;
            return arr = new Array(this);
        }

        /**
         * Converts to object.
         * Throws a JsonException if the type is not convertable
         * @return Object value
         */
        public Object asObject() {
            if (!isObject()) {
                throw new JsonException("Incorrect Type");
            }

            type = Type.NULL;
            return obj = new Object(this);
        }

        /**
         * Constructs a String representation.
         * Since Arrays and Objects are computed lazily, converting them to
         * String requires a pass through which consumes the data, and so since
         * this method has side effects, it is effectively unuseable, but
         * helpful for testing only.
         * @return String representation of this json value
         */
        @Override
        public String toString() {
            switch (type) {
                case NULL:
                    return "null";
                case INT:
                    return String.valueOf(intVal);
                case FLOAT:
                    return String.valueOf(floatVal);
                case BOOLEAN:
                    return String.valueOf(boolVal);
                case STRING:
                    return escapeString(stringVal);
                case ARRAY:
                    {
                        final StringBuilder sb = new StringBuilder();
                        sb.append('[');
                        Iterator<Value> it = asArray();
                        if (it.hasNext()) {
                            sb.append(it.next().toString());
                            it.forEachRemaining(v -> {
                                sb.append(',');
                                sb.append(v.toString());
                            });
                        }
                        sb.append(']');
                        return sb.toString();
                    }
                case OBJECT:
                    {
                        final StringBuilder sb = new StringBuilder();
                        sb.append('{');
                        Iterator<Pair<String, Value>> it = asObject();
                        if (it.hasNext()) {
                            Pair<String, Value> pair = it.next();
                            sb.append(escapeString(pair.getKey()));
                            sb.append(':');
                            sb.append(pair.getValue().toString());
                            it.forEachRemaining(p -> {
                                sb.append(',');
                                sb.append(escapeString(p.getKey()));
                                sb.append(':');
                                sb.append(p.getValue().toString());
                            });
                        }
                        sb.append('}');
                        return sb.toString();
                    }
            }

            throw new RuntimeException("Unreachable code");
        }

        /// Eagerly parses simple values and sets flags for compound values
        private void readValue() {
            switch (chars.getC()) {
                case '{': // doneReading enforces in order traversal
                    type = Type.OBJECT;
                    doneReading = false;
                    return; // no other values may be read until it is set to true

                case '[': // other types set it to true at the end
                    type = Type.ARRAY;
                    doneReading = false;
                    return; // which is why we must return; here

                case '"': // parse a string
                    type = Type.STRING;
                    {
                        StringBuilder sb = new StringBuilder();
                        for (chars.forceNext(); chars.getC() != '"'; chars.forceNext()) {
                            // TODO handle escape chars
                            if (chars.getC() == '\\') {
                                chars.forceNext();
                                switch (chars.getC()) {
                                    case '"':
                                    case '\\':
                                    case '/':
                                        sb.append((char) chars.getC());
                                        break;

                                    case 'b':
                                        sb.append('\b');
                                        break;

                                    case 'n':
                                        sb.append('\n');
                                        break;

                                    case 'r':
                                        sb.append('\r');
                                        break;

                                    case 't':
                                        sb.append('\t');
                                        break;

                                    case 'u':
                                        // TODO unicode
                                    default:
                                        throw new JsonException("Invalid escape char");
                                }
                            } else {
                                sb.append((char) chars.getC());
                            }
                        }
                        stringVal = sb.toString();
                    }
                    chars.skipWhite();
                    break;

                case 'n': // nothing else valid begins with n
                    type = Type.NULL;
                    if (chars.forceNext() != 'u' ||
                            chars.forceNext() != 'l' ||
                            chars.forceNext() != 'l') {
                        throw new JsonException("Unexpected character");
                            }
                    chars.skipWhite();
                    break;

                case ',': // treat empty as null
                    type = Type.NULL;
                    break;


                case 't': // nothing else valid begins with t
                    type = Type.BOOLEAN;
                    boolVal = true;
                    if (chars.forceNext() != 'r' ||
                            chars.forceNext() != 'u' ||
                            chars.forceNext() != 'e') {
                        throw new JsonException("Unexpected character");
                            }
                    chars.skipWhite();
                    break;

                case 'f': // nothing else valid begins with f
                    type = Type.BOOLEAN;
                    boolVal = false;
                    if (chars.forceNext() != 'a' ||
                            chars.forceNext() != 'l' ||
                            chars.forceNext() != 's' ||
                            chars.forceNext() != 'e') {
                        throw new JsonException("Unexpected character");
                            }
                    chars.skipWhite();
                    break;

                default: // either a number or an error
                    if (Character.isDigit(chars.getC()) || chars.getC() == '-') {
                        // first read the number as a string
                        StringBuilder sb = new StringBuilder();
                        do {
                            sb.append((char) chars.getC());
                            chars.next();
                        } while (Character.isDigit(chars.getC()) || chars.getC() == '-' ||
                                chars.getC() == '+' || chars.getC() == '.' ||
                                chars.getC() == 'e' || chars.getC() == 'E');
                        String num = sb.toString();

                        // make sure next character is ready for the next value
                        if (Character.isWhitespace(chars.getC())) {
                            chars.skipWhite();
                        }

                        // check if num is an int or float
                        if (isValidInt(num)) {
                            type = Type.INT;
                            intVal = Integer.parseInt(num);
                        } else if (isValidFloat(num)) {
                            type = Type.FLOAT;
                            floatVal = Double.parseDouble(num);
                        } else {
                            // otherwise it is an error
                            throw new JsonException("Invalid characters in number");
                        }
                    } else {
                        // otherwise it is an error
                        throw new JsonException("Unexpected character");
                    }
            }

            // lazy types return early, simple eager types have finished with
            // the input stream by now
            doneReading = true;
        }

        /**
         * Checks if a string can be converted to an int safely
         * @param String s the number
         * @return true if it can be converted
         */
        private boolean isValidInt(String s) {
            if (s.charAt(0) == '-'  || s.charAt(0) == '+') {
                s = s.substring(1, s.length());
            }
            return s.length() > 0 && s.chars().allMatch(Character::isDigit);
        }

        /**
         * Checks if a string can be converted to a double safely
         * @param String s the number
         * @return true if it can be converted
         */
        private boolean isValidFloat(String s) {
            int dot = s.indexOf('.');
            // allowed either e or E but not both
            int e = Math.max(s.indexOf('e'), s.indexOf('E'));

            // note that neither . nor e are allowed as the first
            // character of a number, and the . must occur before e
            String int_ = dot > 0 ?
                s.substring(0, dot) :
                (e > 0 ? s.substring(0, e) : s);

            if (!isValidInt(int_)) {
                return false;
            }

            if (dot > 0) {
                // since int_ is valid int, if e and dot both exist
                // it must be that dot < e if we got this far
                String frac = e > 0 ?
                    s.substring(dot + 1, e) :
                    s.substring(dot + 1, s.length());

                if (frac.length() == 0 ||
                        !frac.chars().allMatch(Character::isDigit)) {
                    return false;
                        }
            }

            if (e > 0) {
                String exp = s.substring(e + 1, s.length());
                if (!isValidInt(exp)) {
                    return false;
                }
            }

            return true;
        }
    }

    private static String escapeString(String stringVal) {
        final StringBuilder s = new StringBuilder();
        s.append('"');
        stringVal.chars().forEach(c -> {
            switch (c) {
                case '\b':
                    s.append('\\');
                    s.append('b');
                    break;
                case '\n':
                    s.append('\\');
                    s.append('n');
                    break;
                case '\r':
                    s.append('\\');
                    s.append('r');
                    break;
                case '\t':
                    s.append('\\');
                    s.append('t');
                    break;

                case '\"':
                case '\'':
                case '\\':
                    s.append('\\');

                default:
                    s.append((char) c);
                    break;
            }
        });
        s.append('"');
        return s.toString();
    }

    /**
     * Compound array type.
     * Arrays are stream like objects which can pass through the input stream
     * only once. Arrays are Iterators and can be converted to Streams for
     * functional style computations. Each Value from within the Array (if it is
     * also a lazy type such as Array or Object) MUST be completely processed
     * before the next Value can be requested.
     */
    public static final class Array implements Iterator<Value> {

        /// the Value I came from
        private final Value this_;
        /// input stream
        private final CharStream chars;
        // use to enforce in order traversal
        private Value current;

        /// control flags
        private boolean hasNext;
        private boolean dirty;

        /**
         * Construct an Array from it's Value object
         */
        private Array(Value v) {
            this_ = v;
            this.chars = this_.chars;

            if (chars.forceSkipWhite() != ']') {
                hasNext = true;
            } else {
                this_.doneReading = true;
                hasNext = false;
                chars.forceSkipWhite();
            }
        }

        /**
         * Checks if the array has any more entries
         * @return false if array is empty
         */
        @Override
        public boolean hasNext() {
            if (dirty) {
                checkNext();
            }

            return hasNext;
        }

        /**
         * Gets the next value in the array
         * @return Value the next value
         */
        @Override
        public Value next() {
            if (!hasNext()) {
                throw new RuntimeException("next() called on empty iterator");
            }

            current = new Value(chars);
            dirty = true;

            return current;
        }

        /**
         * Converts this object to Stream to be used with java's Stream api
         * @return Stream<Value> stream of parsed values
         */
        public Stream<Value> stream() {
            Iterable<Value> iter = () -> this;
            return StreamSupport.stream(iter.spliterator(), false);
        }

        /**
         * Consumes the rest of the input for this array by iterating over each
         * entry and recursively clearing them until they are finished
         * with their input. 
         */
        public void clear() {
            forEachRemaining(v -> {
                if (v.isArray()) {
                    v.asArray().clear();
                } else if (v.isObject()) {
                    v.asObject().clear();
                }
            });
        }

        /**
         * Checks if there are remaining entries in the array and clears the
         * dirty flag. 
         */
        private void checkNext() {
            if (current != null && !current.doneReading) {
                if (current.isArray()) {
                    current.asArray().clear();
                } else if (current.isObject()) {
                    current.asObject().clear();
                } else if (current.arr != null) {
                    current.arr.clear();
                } else if (current.obj != null) {
                    current.obj.clear();
                }
                current = null;
            }

            if (chars.getC() == ']') {
                chars.skipWhite();
                this_.doneReading = true;
                hasNext = false;
            } else if (chars.getC() == ',') {
                chars.forceSkipWhite();
                hasNext = true;
            } else {
                throw new JsonException("Unexpected character");
            }

            dirty = false;
        }
    }

    /**
     * Stream representation of json objects.
     * Objects can be converted to Streams for functional style computations. 
     * Each Value from within the Object  MUST be completely processed before 
     * the next String Value pair can be requested.
     */
    public static final class Object implements Iterator<Pair<String, Value>> {
        /// the Value I came from
        private final Value this_;
        /// input stream
        private final CharStream chars;
        // use to enforce in order traversal
        private Value current;

        /// control flags
        private boolean hasNext;
        private boolean dirty;

        /**
         * Construct an Object from it's Value object
         */
        private Object(Value v) {
            this_ = v;
            chars = this_.chars;

            if (chars.forceSkipWhite() == '"') {
                hasNext = true;
            } else if (chars.getC() == '}') {
                chars.skipWhite();
                this_.doneReading = true;
                hasNext = false;
            } else {
                throw new JsonException("Expected string key or }");
            }
        }

        /**
         * Checks if the object has any more entries
         * @return false if object is empty
         */
        @Override
        public boolean hasNext() {
            if (dirty) {
                checkNext();
            }

            return hasNext;
        }

        /**
         * Gets the next pair in the array
         * @return Pair<String,Value> the next key value pair
         */
        @Override
        public Pair<String, Value> next() {
            if (!hasNext()) {
                throw new RuntimeException("next() called on empty iterator");
            }

            Value key = new Value(chars);
            if (chars.getC() != ':') {
                throw new JsonException("Expected `:` between key and value");
            }
            chars.forceSkipWhite();

            current = new Value(chars);
            dirty = true;

            // type inference
            return new Pair<>(key.asString(), current);
        }

        /**
         * Converts this object to Stream to be used with java's Stream api
         * @return Stream<Pair<String,Value>> stream of parsed key value pairs
         */
        public Stream<Pair<String, Value>> stream() {
            Iterable<Pair<String, Value>> iter = () -> this;
            return StreamSupport.stream(iter.spliterator(), false);
        }

        /**
         * Consumes the rest of the input for this object by iterating over each
         * entry and recursively clearing them until they are finished
         * with their input. 
         */
        public void clear() {
            forEachRemaining(p -> {
                Value v = p.getValue();
                if (v.isArray()) {
                    v.asArray().clear();
                } else if (v.isObject()) {
                    v.asObject().clear();
                }
            });
        }

        /**
         * Checks if there are remaining entries in the object and clears the
         * dirty flag. 
         */
        private void checkNext()  {
            if (current != null && !current.doneReading) {
                if (current.isArray()) {
                    current.asArray().clear();
                } else if (current.isObject()) {
                    current.asObject().clear();
                } else if (current.arr != null) {
                    current.arr.clear();
                } else if (current.obj != null) {
                    current.obj.clear();
                }
                current = null;
            }

            if (chars.getC() == '}') {
                chars.skipWhite();
                this_.doneReading = true;
                hasNext = false;
            } else if (chars.getC() == ',') {
                chars.forceSkipWhite();
                if (chars.getC() != '"') {
                    throw new JsonException("Expected string key");
                }
                hasNext = true;
            } else {
                throw new JsonException("Unexpected character");
            }

            dirty = false;
        }
    }

}
