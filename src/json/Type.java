
package json;

/**
 * Type which json Values can take on
 */
public enum Type {
    NULL, INT, FLOAT, BOOLEAN, STRING, ARRAY, OBJECT
}

