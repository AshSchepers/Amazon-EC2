
package json;

/**
 * Utility class for iterating over json Objects.
 * Tries to copy javafx.util.Pair
 */
public class Pair<K, V> {
    private K key;
    private V val;

    /**
     * Constructs a pair
     */
    public Pair(K key, V val) {
        this.key = key;
        this.val = val;
    }

    /**
     * Gets the first entry (car)
     * @return K key
     */
    public K getKey() {
        return key;
    }

    /**
     * Gets the second entry (cdr)
     * @return V value
     */
    public V getValue() {
        return val;
    }
}

