
package json;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


/**
 * Exception used for malformed input and out of order traversal
 */
public class JsonException extends RuntimeException {
    /**
     * Stops the compiler complaining
     */
    public static final long serialVersionUID = 1532661L;

    /**
     * Constructor forwards to super
     */
    public JsonException(String s) {
        super(s);
    }
}


