from flask import Flask, request, session, redirect, url_for, render_template, flash
from flask_login import LoginManager, login_required, login_user, logout_user, login_fresh
import re
import os
from flask_oauth import OAuth
from json import dumps
from user import User
from warrant import Cognito
from functions import *
import logging
import logging.config
from cognitomethods import *
import boto3
boto3.setup_default_session(region_name='eu-west-1')


app = Flask(__name__)
app.jinja_env.globals.update(login_fresh=login_fresh)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
app.secret_key = os.urandom(50)

BASE_DIR=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# logging off for different directory
#logging.config.fileConfig(os.path.join(BASE_DIR, 'amazon', 'logging.conf'))
#bookmark_logger = logging.getLogger('bookmark')
#compare_logger = logging.getLogger('compare')
#purchase_logger = logging.getLogger('purchase')
selected = dict()
ip_admin = "10.7.255.255"

@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

def get_form_vals_instances(request):
    name = request.form['name']
    _type = request.form['InstanceType_type']
    vCPUs = request.form['vCPUs']
    memoryKB = request.form['memoryKB']
    features = request.form['features'].split(', ')
    virtualizations = request.form['virtualizations'].split(', ')
    architectures = request.form['architectures'].split(', ')
    placementGroupsSupported = request.form['placementGroupsSupported']
    rootDeviceTypes = request.form['rootDeviceTypes'].split(', ')
    ephemeralsSupported = request.form['ephemeralsSupported']
    publicFacing = request.form['publicFacing']
    dedicatedTenancySupported = request.form['dedicatedTenancySupported']
    ipv6Supported = request.form['ipv6Supported']
    egpuSupported = request.form['egpuSupported']
    platforms = request.form['platforms'].split(', ')
    hypervisors = request.form['hypervisors'].split(', ')
    CpuOptions_type = request.form['CpuOptions_type']
    threadsPerCore = request.form['threadsPerCore'].split(', ')
    coreCounts = request.form['coreCounts'].split(', ')
    defaultThreadsPerCore = request.form['defaultThreadsPerCore']
    defaultCoreCount = request.form['defaultCoreCount']

    cpuOptions = {"_type": CpuOptions_type, "threadsPerCore": threadsPerCore, "coreCounts": coreCounts, "defaultThreadsPerCore": defaultThreadsPerCore, "defaultCoreCount": defaultCoreCount}
    setter ={"name": name, "_type": _type, "vCPUs": vCPUs, "memoryKB": memoryKB, "features": features, "virtualizations": virtualizations, "architectures": architectures, "placementGroupsSupported": placementGroupsSupported, "rootDeviceTypes": rootDeviceTypes, "ephemeralsSupported": ephemeralsSupported, "publicFacing": publicFacing, "dedicatedTenancySupported": dedicatedTenancySupported, "ipv6Supported": ipv6Supported, "egpuSupported": egpuSupported, "platforms": platforms, "hypervisors": hypervisors, "cpuOptions": cpuOptions}
    return setter

@app.route('/')
@app.route('/index')
def index():
    vCPUs = get_vals_of('vCPUs')
    memoryKB = get_vals_of('memoryKB')
    features = get_vals_of_list('features')
    threadsPerCore = get_vals_of_object('cpuOptions', 'threadsPerCore')
    coreCounts = get_vals_of_object('cpuOptions', 'coreCounts')
    instances = get_instances()
    bookmarked = []
    price = ''

    myselected = set()
    if '_id' in session and session['_id'] in selected:
        myselected = selected[session['_id']]

    if session.get('username'):
        bookmarked = User(session['username']).getBookmarked()
        price = User(session['username']).getPrice()

    return render_template('index.html', vCPUs=vCPUs, memoryKB=memoryKB, features=features, threadsPerCore=threadsPerCore, coreCounts=coreCounts, instances=instances, bookmarked=bookmarked, price=price, myselected=myselected)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        if not request.form.get('username') or not request.form.get('password'):
            flash('Please enter your credentials!')
        else:    
            username = request.form['username']
            password = request.form['password']
            user = User(username)
            if not user.verify(password):
                flash('Invalid login credentials!')
            else:
                if user.isUnverified():
                    session['type'] = 2
                elif user.isVerified():
                    session['type'] = 1
                else:
                    session['type'] = 0
                session['username'] = username
                login_user(user)
                selected[session['_id']] = set()
                
                return redirect(request.args.get("next") or url_for('index'))
        if session.get('username'):
            return redirect(url_for('index'))
    return render_template('login.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        password = request.form['password']

        uword = "^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){2,18}$"
        eword = "(?=.*\w).+@\w+\..+"
        pword = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!\.\?,]).{6,}"

        person = User(username)

        if not re.match(uword, username):
            flash('Your username is not of the correct format.')
        elif len(firstname) < 1:
            flash('Your firstname must be at least one character.')
        elif len(lastname) < 1:
            flash('Your lastname must be at least one character.')
        elif not re.match(eword, email):
            flash('Incorrect email format.')
        elif not re.match(pword, password):
            flash('Password invalid!')
        elif len(person.get_user()) > 0:
            flash('Username already exists')
        elif not person.valid_email(email):
            flash('Email already taken!')
        else:
            try:
                if (person.register(password, email, firstname, lastname)):
                    registerUser(email, username, password)
                    return redirect(url_for('login'))
            except:
                flash('Account creation failed')

    if session.get('username') and login_fresh():
        return redirect(url_for('index'))
    return render_template('registration.html')

@app.route('/confirm', methods=['GET', 'POST'])
@login_required
def confirm():
    if request.method == 'POST':
        conf_code = request.form['verification']
        try:
            confirmSignUp(conf_code, session['username'])
            user = User(session['username']).verifyUser()
            
            return redirect(url_for('login'))
        except:
            return render_template('confirm.html')

@app.route('/logout')
@login_required
def logout():
    selected.pop(session['_id'], None)
    print('before logout --------------------')
    print(session)
    logout_user()
    print('after logout --------------------')
    print(session)
    return redirect(url_for('index'))

@app.route('/dashboard')
@login_required
def dashboard():
    if session.get('username'):
        user = User(session['username'])
        if user.isUnverified():
            return render_template('verify.html')
        elif user.isVerified():
            #user = User(session['username'])
            info = user.get_user()[0]
            username = info['username']
            firstname = info['first_name']
            lastname = info['last_name']
            email = info['email']
            payment = info['payment_currency']
            prices = get_prices()
            print ("Payment##")
            print (payment)
            print (prices)
            return render_template('dashboard.html', username=username,firstname=firstname,lastname=lastname, email=email, payment=payment, prices=prices)
    return redirect(url_for('index'))

@app.route('/update_info', methods=['GET', 'POST'])
@login_required
def update_info():
    if request.method == 'POST':
        print(request.form)
        if (request.form.get('firstname') and request.form.get('lastname') and request.form.get('email')):
            user = User(session['username'])
            user.update_info(request.form['firstname'], request.form['lastname'], request.form['email'])	
        elif (request.form.get('payment')):
            user = User(session['username'])
            user.update_payment(request.form['payment'])
        else:
            flash('Invalid input!')
    return redirect(url_for('dashboard'))

@app.route('/verify', methods=['GET', 'POST'])
@login_required
def verify_me():
    return render_template('verify.html')    

@app.route('/verify_email', methods=['GET', 'POST'])
@login_required
def verify_email():
    if request.method == 'POST':
        if (request.form.get('conf_code')):
            username = session['username']
            print(username)
            try:
                confirmSignUp(request.form['conf_code'], username)
                user = User(username)
                user.verifyUser()
                session['type'] = 1
            except:
                print('exception')   
        else:
            flash('Invalid input!')
    return redirect(url_for('index'))

@app.route('/bookmark/<place>', methods=['GET', 'POST'])
@login_required
def bookmark(place='index'):
    user = User(session['username'])
    user.bookmark(request.form['bookmark'])

    curr_instance = get_instance(request.form['bookmark'])
    print('----------------')
    print(curr_instance)
    print('----------------')
    try:
        name  = 'name: ' + curr_instance['Item']['name']
        mem   = ' - memoryKB: ' + str(curr_instance['Item']['memoryKB'])
        if curr_instance['Item']['features'] != '':
            feats = ' - features: ' + ', '.join(curr_instance['Item']['features'])
        else:
            feats = ' - features: none'
        core  = ' - coreCounts: ' + ', '.join(str(val) for val in curr_instance['Item']['cpuOptions']['coreCounts'])
        thrd  = ' - threadsPerCore: ' + ', '.join(str(val) for val in curr_instance['Item']['cpuOptions']['threadsPerCore'])
        hype  = ' - hypervisors: ' + ', '.join(curr_instance['Item']['hypervisors'])
        cashe = ' - price: '
    
        if session.get('username'):
            if curr_instance['Item']['pricing'] != 'null':
                if User(session['username']).getPrice() in curr_instance['Item']['pricing']:
                        cashe = cashe + '$' + curr_instance['Item']['pricing'][User(session['username']).getPrice()]
            else:
                cashe = cashe + "$0" 
    
        if cashe == ' - price: ':
            cashe = ' - price: $0'

        if mem == ' - memoryKB: None ':
            mem = ' - memoryKB: 0 '

        #bookmark_logger.info(name + mem + feats + core + thrd + hype + cashe)
    except:
        pass

    return redirect(url_for('index'))
    

@app.route('/unbookmark/<place>', methods=['GET', 'POST'])
@login_required
def unbookmark(place='index'):
    user = User(session['username'])
    user.unbookmark(request.form['unbookmark'])
    return redirect(url_for('index'))
   

@app.route('/bookmarked')
@login_required
def bookmarked():
    vCPUs = get_vals_of('vCPUs')
    memoryKB = get_vals_of('memoryKB')
    features = get_vals_of_list('features')
    threadsPerCore = get_vals_of_object('cpuOptions', 'threadsPerCore')
    coreCounts = get_vals_of_object('cpuOptions', 'coreCounts')
    instances = get_instances()
    bookmarked = []
 
    myselected = set()
    if '_id' in session and session['_id'] in selected:
        myselected = selected[session['_id']]

    price = ''

    if session.get('username'):
        bookmarked = User(session['username']).getBookmarked()
        price = User(session['username']).getPrice()

    return render_template('bookmarked.html', vCPUs=vCPUs, memoryKB=memoryKB, features=features, threadsPerCore=threadsPerCore, coreCounts=coreCounts, instances=instances, bookmarked=bookmarked, myselected=myselected, price=price)

@app.route('/change_password', methods=['GET', 'POST'])
@login_required
def change_password():
    if request.method == 'POST':
        username = session['username']
        if (request.form.get('oldpassword') or request.form.get('newpassword')):
            oldpassword = request.form['oldpassword']
            newpassword = request.form['newpassword']
            pword = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!\.\?,]).{6,}"
            if not re.match(pword, oldpassword):
                flash('Old password incorrect!')
            elif not re.match(pword, newpassword):
                flash('New password invalid!')
            else:
                person = User(username)
                person.change_password(oldpassword, newpassword)
                return redirect(url_for('index'))
        flash('Invalid input!')
    if 'type' in session:
        print "THIS IS SESSION"
        print (session)
        if session.get('type') < 2:
            return render_template('changepassword.html')    
    return redirect(url_for('index'))


@app.route('/adm/admin_login', methods=['GET', 'POST'])
def admin_login():
    #if request.remote_addr:
    #    if request.remote_addr != ip_admin:
    #        return redirect(url_for('index'))
    if request.method == 'POST':
        print("ADMIN LOGIN")
        username = request.form['username']
        password = request.form['password']
        if not User(username).verify(password):
            flash('Invalid login credentials!')
        else:
            if User(username).isAdmin():
                print ("I am admin")
                session['username'] = username
                session['type'] = 0

                login_user(User(username))
                selected[session['_id']] = set()

                return redirect(url_for('admin_panel'))
            else:
                flash("Not an admin!")
    return render_template('admin_login.html')

@app.route('/adm/admin_panel')
@login_required
def admin_panel():
    #if request.remote_addr:
    #    if request.remote_addr != ip_admin:
    #        return redirect(url_for('index'))
    vCPUs = get_vals_of('vCPUs')
    memoryKB = get_vals_of('memoryKB')
    features = get_vals_of_list('features')
    threadsPerCore = get_vals_of_object('cpuOptions', 'threadsPerCore')
    coreCounts = get_vals_of_object('cpuOptions', 'coreCounts')
    instances = get_instances()

    return render_template('admin_panel.html', instances=instances, vCPUs=vCPUs, memoryKB=memoryKB, features=features, threadsPerCore=threadsPerCore, coreCounts=coreCounts)

@app.route('/add_instance', methods=['GET', 'POST'])
@login_required
def add_instance():
    if request.remote_addr:
    #    if request.remote_addr != ip_admin:
    #        return redirect(url_for('index'))
        if request.method == 'POST':
            setter = get_form_vals_instances(request)
            insert_instance(setter)
            return redirect(url_for('admin_panel'))

@app.route('/change_instance/<iname>', methods=['GET', 'POST'])
@login_required
def change_instance(iname):
    if request.remote_addr:
    #    if request.remote_addr != ip_admin:
    #        return redirect(url_for('index'))
        if request.method == 'POST':
            if request.form['change_instance'] == 'delete':
                delete_instance(iname)
                return redirect(url_for('admin_panel'))
            elif request.form['change_instance'] == 'update':
                print(iname)
                setter = get_form_vals_instances(request)

                print('--------------')
                print(setter)
                update_instance(iname, setter)
    return redirect(url_for('admin_panel'))


@app.route('/compare_selected')
@login_required
def compare_selected():
    vCPUs = get_vals_of('vCPUs')
    memoryKB = get_vals_of('memoryKB')
    features = get_vals_of_list('features')
    threadsPerCore = get_vals_of_object('cpuOptions', 'threadsPerCore')
    coreCounts = get_vals_of_object('cpuOptions', 'coreCounts')
    instances = get_instances()

    for val in selected[session['_id']]:
        print(val)
        curr_instance = get_instance(val)
        try:
            name  = 'name: ' + curr_instance['Item']['name']
            mem   = ' - memoryKB: ' + str(curr_instance['Item']['memoryKB'])

            if curr_instance['Item']['features'] != '':
                feats = ' - features: ' + ', '.join(curr_instance['Item']['features'])
            else:
                feats = ' - features: none'

            core  = ' - coreCounts: ' + ', '.join(str(val) for val in curr_instance['Item']['cpuOptions']['coreCounts'])
            thrd  = ' - threadsPerCore: ' + ', '.join(str(val) for val in curr_instance['Item']['cpuOptions']['threadsPerCore'])
            hype  = ' - hypervisors: ' + ', '.join(curr_instance['Item']['hypervisors'])
            cashe = ' - price: '

            if session.get('username'):
                if curr_instance['Item']['pricing'] != 'null':
                    print('-----------------')
                    print(curr_instance)
                    print('-----------------')
                    if User(session['username']).getPrice() in curr_instance['Item']['pricing']:
                        cashe = cashe + '$' + curr_instance['Item']['pricing'][User(session['username']).getPrice()]
                else:
                    cashe = cashe + "$0" 

            if cashe == ' - price: ':
                cashe = ' - price: $0'

            if mem == ' - memoryKB: None ':
                mem = ' - memoryKB: 0 '

            #compare_logger.info(name + mem + feats + core + thrd + hype + cashe)
        except:
            pass

    if not session['_id'] in selected:
        selected[session['_id']] = set()

    price = ''
    if session.get('username'):
        price = User(session['username']).getPrice()

    if session.get('username'):
        bookmarked = User(session['username']).getBookmarked()
    return render_template('selected.html', vCPUs=vCPUs, memoryKB=memoryKB, features=features, threadsPerCore=threadsPerCore, coreCounts=coreCounts, instances=instances, bookmarked=bookmarked, selected=selected[session['_id']], price=price)

@app.route('/select/<myselect>')
@login_required
def select(myselect):
    print('----------SELECTED--------------')
    print(myselect)
    if '_id' in session and session['_id'] in selected:
        if myselect in selected[session['_id']]:
            selected[session['_id']].remove(myselect)
        else:
            selected[session['_id']].add(myselect)
    elif '_id' in session:
        selected[session['_id']] = set(myselect)
    else:
        return redirect(url_for('index'))

    return dumps({'selected': selected})

@app.route('/privacy')
def privacy_policy():
    return render_template('privacy.html')

def main():
    app.run()
#u = Cognito('eu-west-1_JZy1oER4Y', 'oocrfemfk0mhvi5p1jirbi3g1', username='kyledavey1337@gmail.com')
#print (u.authenticate(password='Pumpkin123!'))
#print (u)
print ("my dood")
if __name__ == '__main__':
    main()
