#https://github.com/capless/warrant
from warrant import Cognito
import boto3
boto3.setup_default_session(region_name='eu-west-1')

#youruserpoolid = 'eu-west-1_JZy1oER4Y'
#yourclientid = 'oocrfemfk0mhvi5p1jirbi3g1'
youruserpoolid = 'eu-west-1_BctWqYfnI'
yourclientid = '3j66s6u3ik63kpsfc0stt3amfk'

def registerUser(email_addr, username, password):
	u = Cognito(youruserpoolid, yourclientid)
	u.add_base_attributes(email=email_addr, name=username)
	u.register(username, password)

def authenticate_user(username, user_password):
	u = Cognito(youruserpoolid, yourclientid, username)
	u.authenticate(password=user_password)

def adminAuthenticate_user(username, user_password):
	u = Cognito(youruserpoolid, yourclientid, username)
	u.admin_authenticate(password=user_password)
	
def initForgotPassword(my_username):
	u = Cognito(youruserpoolid, yourclientid, username=my_username)
	u.initiate_forgot_password()

def confirmForgotPassword(conf_code, new_password, my_username):
	u = Cognito(youruserpoolid, yourclientid, username=my_username)
	u.confirm_forgot_password(conf_code, new_password)

def changePassword(old_password, new_password):
	u = Cognito(youruserpoolid,yourclientid,
		id_token='id-token',refresh_token='refresh-token',
		access_token='access-token')
	u.change_password(old_password, new_password)

def confirmSignUp(conf_code, my_username):
	u = Cognito(youruserpoolid, yourclientid)
	u.confirm_sign_up(conf_code,username=my_username)

def updateProfile(name, family_name):
	u = Cognito(youruserpoolid,yourclientid,
		id_token='id-token',refresh_token='refresh-token',
		access_token='access-token')
	u.update_profile({'given_name':name,'family_name':family_name,},attr_map=dict())

def sendVerification(email, my_username, my_password):
	user = Cognito(youruserpoolid, yourclientid, username=my_username)
	user.authenticate(password=my_password)
	u = Cognito(youruserpoolid,yourclientid,
		id_token=user.id_token,refresh_token=user.refresh_token,
		access_token=user.access_token)
	u.send_verification(attribute='stefanvd44@gmail.com')

def getUserObject(my_username):
	u = Cognito(youruserpoolid,yourclientid,
		id_token='id-token',refresh_token='refresh-token',
		access_token='access-token')

	u.get_user_obj(username=my_username,
		attribute_list=[{'Name': 'string','Value': 'string'},],
		metadata={},
		attr_map={"given_name":"first_name","family_name":"last_name"}
		)

def getUser(my_username):
	u = Cognito(youruserpoolid, yourclientid, username=my_username)
	user = u.get_user(attr_map={"given_name":"first_name","family_name":"last_name"})

def getUsers():
	u = Cognito(youruserpoolid, yourclientid)
	user = u.get_users(attr_map={"given_name":"first_name","family_name":"last_name"})

def getGroupObj():
	u = Cognito(youruserpoolid, yourclientid)
	group_data = {'GroupName': 'user_group', 'Description': 'description',
		'Precedence': 1}
	group_obj = u.get_group_obj(group_data)

def checkToken():
	u = Cognito(youruserpoolid,yourclientid,
		id_token='id-token',refresh_token='refresh-token',
		access_token='access-token')
	u.check_token()

def logOut():
	u = Cognito(youruserpoolid,yourclientid,
		id_token='id-token',refresh_token='refresh-token',
		access_token='access-token')
	u.logout()

#u = Cognito(youruserpoolid, yourclientid, username='kyledavey1337@gmail.com')
#print (u.authenticate(password='Pumpkin123!'))
#print (u)
