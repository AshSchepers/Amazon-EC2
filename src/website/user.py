from passlib.hash import bcrypt
from boto3.dynamodb.conditions import Key, Attr

import app

dynamo = app.get_dynamo()
class User:
    def __init__(self, user):
        print (str("The user is: ") + str(user))
        self.user = user

    def get_user(self):
        response = dynamo.tables['users'].query(
            KeyConditionExpression=Key('username').eq(self.user)
        )
        return response['Items']

    def verify(self, password):
        user = self.get_user()
        if len(user):
            print("username exists")
            return bcrypt.verify(password, user[0]['password'])
        else:
            print("username does not exist")
            return False

    def is_authenticated(self):
        return self.isVerified()

    def is_active(self):
        return self.isVerified()

    def is_anonymous(self):
        # TODO possible change for allowing anonymous users with id 1-
        return False

    def get_id(self):
        return self.user

    def isAdmin(self):
        user = self.get_user()
        response = dynamo.tables['users'].query(
            KeyConditionExpression=Key('username').eq(self.user)
        )
        admin = dynamo.tables['groups'].query(
            KeyConditionExpression=Key('name').eq('admin')
        )
        return response['Items'][0]['group'] == admin['Items'][0]['type']
    
    def isVerified(self):
        user = self.get_user()
        response = dynamo.tables['users'].query(
            KeyConditionExpression=Key('username').eq(self.user)
        )
        nonverified = dynamo.tables['groups'].query(
            KeyConditionExpression=Key('name').eq('non_verified')
        )
        return response['Items'][0]['group'] < nonverified['Items'][0]['type']
    
    def verifyUser(self):
        user = self.get_user()
        grp = 1;
        dynamo.tables['users'].update_item(
            Key={
                'username': self.user
            },
            UpdateExpression="SET #group = :g",
            ExpressionAttributeValues={
                ':g': 1,
            },
            ExpressionAttributeNames={
                "#group": "group"
            })
        

    def isUnverified(self):
        user = self.get_user()
        response = dynamo.tables['users'].query(
            KeyConditionExpression=Key('username').eq(self.user)
        )
        admin = dynamo.tables['groups'].query(
            KeyConditionExpression=Key('name').eq('non_verified')
        )
        return response['Items'][0]['group'] == admin['Items'][0]['type']

    def change_password(self, old_password, new_password):
        if self.verify(old_password):
            setNewPassword = dynamo.tables['users'].update_item(
                Key={
                    'username': self.user
                },
                UpdateExpression="SET password = :p",
                ExpressionAttributeValues={
                    ':p': bcrypt.encrypt(new_password),
                })
        else:
            print ('Failed to change password')
    
    def update_info(self, firstname, lastname, email):
        dynamo.tables['users'].update_item(
            Key={
                'username': self.user
            },
            UpdateExpression="SET first_name = :f, last_name = :l, email = :e",
            ExpressionAttributeValues={
                ':f': firstname,
                ':l': lastname,
                ':e': email,
            }
        )
        
    def update_payment(self, payment):
        dynamo.tables['users'].update_item(
            Key={
                'username': self.user
            },
            UpdateExpression="SET payment_currency = :p",
            ExpressionAttributeValues={
                ':p': payment,
            }
        )
                

    def register(self, password, email, firstname, surname):
        user = self.get_user()
        if not len(self.get_user()):
            dynamo.tables['users'].put_item(
                Item={
                    'username': self.user,
                    'first_name': firstname,
                    'last_name': surname,
                    'email': email,
                    'password': bcrypt.encrypt(password),
                    'group': 2,
                    'payment_currency': ' ',
                    'bookmarked': []
                }
            )   
            return True
        return False
            
    def valid_email(self, email):
        valid = True
        emails = dynamo.tables['users'].scan(
            ProjectionExpression='email'
        )
        emails = emails['Items']
        for e in emails:
            if e['email'] == email:
                valid = False
        return valid

    def bookmark(self, instance):
        print ("Bookmark called!")
        dynamo.tables['users'].update_item(
            Key={
                'username': self.user
            },
            UpdateExpression="SET bookmarked = list_append(bookmarked, :t)",
            ExpressionAttributeValues={
                ':t': [instance],
            }
        )

    def unbookmark(self, instance):
        print ("Unbookmark called!")
        lst = self.get_user()[0]['bookmarked']
        lst.remove(instance)
        dynamo.tables['users'].update_item(
            Key={
                'username': self.user
            },
            UpdateExpression="SET bookmarked = :b",
            ExpressionAttributeValues={
                ':b': lst,
            }
        )

    def getBookmarked(self):
        user = self.get_user()
        return user[0]['bookmarked']

    def getPrice(self):
        user = self.get_user()
        return user[0]['payment_currency']

