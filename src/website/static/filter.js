$(document).ready(function() {
    var minVcpus    = 0;
    var maxVcpus    = Number.MAX_SAFE_INTEGER;
    var minMemory   = 0;
    var maxMemory   = Number.MAX_SAFE_INTEGER;
    var minThreads  = 0;
    var maxThreads  = Number.MAX_SAFE_INTEGER;
    var minCores    = 0;
    var maxCores    = Number.MAX_SAFE_INTEGER;
    var iops        = false;
    var piops       = false;
    var searchValue = '';

    // from https://github.com/thinkphp/String.levenshtein/blob/master/Source/String.levenshtein.js but no depencencies
    function levenshtein(str1, str2) {
        var cost = new Array(),
        n = str1.length,
        m = str2.length,
        i, j;

        var minimum = function(a, b, c) {
            var min = a;
            if (b < min) {
                min = b;
            }
            if (c < min) {
                min = c;
            }
            return min;
        }

        if (n == 0) {
            return;
        }

        if (m == 0) {
            return;
        }

        for (var i = 0; i <= n; i++) {
            cost[i] = new Array();
        }

        for (i = 0; i <= n; i++) {
            cost[i][0] = i;
        }

        for (j = 0; j <= m; j++) {
            cost[0][j] = j;
        }

        for (i = 1; i <= n; i++) {
            var x = str1.charAt(i - 1);

            for (j = 1; j <= m; j++) {
                var y = str2.charAt(j - 1);

                if (x == y) {
                    cost[i][j] = cost[i - 1][j - 1];
                } else {
                    cost[i][j] = 1 + minimum(cost[i - 1][j - 1], cost[i][j - 1], cost[i - 1][j]);
                }

            } //endfor

        } //endfor

        return cost[n][m];
    }

    $('#table-instances tbody').on('click', 'tr', function() {
        if ($(this).attr('name') != 'no-selecting') {
            $(this).toggleClass('selected');
            if ($(this).is('.selected')) {
                $(this).attr('name', 'select-compare');
            } else {
                $(this).attr('name', 'no-select-compare');
            }
        }
    });

    function changeTable() {
        if ($('#table-instances').length) {
            changeTableIndex();
        }
        else {
            changeTableAdmin();
        }      
    }

    function changeTableIndex() {
        $('#table-instances tr').each(function() {
            var instanceValue = $(this).find(".Instance-name").html();
            var vcpuValue     = $(this).find(".vcpu-value").html();
            var memoryValue   = $(this).find(".memory-value").html();
            var threadsValue  = $(this).find(".threads-value").html();
            var coresValue    = $(this).find(".cores-value").html();
            var iopsValue     = $(this).find(".iops-value").html();
            var piopsValue    = $(this).find(".piops-value").html();

            if (vcpuValue) {
                var dist = levenshtein(searchValue, instanceValue);
                var threads = threadsValue.split(',');
                var cores   = coresValue.split(',');

                iopsValue = iopsValue != "Not Available";
                piopsValue = piopsValue != "Not Available";

                if (parseInt(maxVcpus, 10) < parseInt(vcpuValue, 10) 
                        || parseInt(vcpuValue, 10) < parseInt(minVcpus, 10)
                        || parseInt(memoryValue, 10) < parseInt(minMemory, 10)
                        || parseInt(maxMemory, 10) < parseInt(memoryValue, 10)
                        || threads.every(function(t) 
                            { return parseInt(t, 10) < parseInt(minThreads, 10)
                                || parseInt(t, 10) > parseInt(maxThreads, 10); })
                        || cores.every(function(t) 
                            { return parseInt(t, 10) < parseInt(minCores, 10)
                                || parseInt(t, 10) > parseInt(maxCores, 10); })
                        || (iops && !iopsValue) || (piops && !piopsValue)
                        || (dist > 5 && !instanceValue.startsWith(searchValue, 0))) {
                    $(this).css("display", "none");
                } else {
                    $(this).css("display", "table-row");
                }
            }
        });
    }

    function changeTableAdmin() {
        $('#table-instances-admin tr').each(function() {
            var instanceValue = $(this).find('input[name="name"]').val();
            var vcpuValue     = $(this).find('input[name="vCPUs"]').val();
            var memoryValue   = $(this).find('input[name="memoryKB"]').val();
            var threadsValue  = $(this).find('input[name="threadsPerCore"]').val();
            var coresValue    = $(this).find('input[name="coreCounts"]').val();
            var featureValue  = $(this).find('input[name="features"]').val();


            if (vcpuValue && vcpuValue != "*") {
                var dist = levenshtein(searchValue, instanceValue);
                var threads = threadsValue.split(',');
                var cores   = coresValue.split(',');

                var iopsValue = ((featureValue.includes ("iops") && !featureValue.includes ("piops")) || (featureValue.includes ("iops") && featureValue.includes (",")));
                var piopsValue = featureValue.includes ("piops");

                if (parseInt(maxVcpus, 10) < parseInt(vcpuValue, 10) 
                        || parseInt(vcpuValue, 10) < parseInt(minVcpus, 10) 
                        || parseInt(memoryValue, 10) < parseInt(minMemory, 10) 
                        || parseInt(maxMemory, 10) < parseInt(memoryValue, 10) 
                        || threads.every(function(t) 
                            { return parseInt(t, 10) < parseInt(minThreads, 10) 
                                || parseInt(t, 10) > parseInt(maxThreads, 10); })
                        || cores.every(function(t) 
                            { return parseInt(t, 10) < parseInt(minCores, 10) 
                                || parseInt(t, 10) > parseInt(maxCores, 10); })
                        || (iops && !iopsValue) || (piops && !piopsValue) 
                        || (dist > 5 && !instanceValue.startsWith(searchValue, 0))) {
                    $(this).css("display", "none");
                } else {
                    $(this).css("display", "table-row");
                }
            }
        });
    }   

    $('#minVcpus').change(function() {
        //Selected value
        var inputValue = $(this).val();
        if (inputValue == "None") {
            minVcpus = 0;
        } else {
            minVcpus = inputValue;
        }

        changeTable();
    });

    $('#maxVcpus').change(function() {
        //Selected value
        var inputValue = $(this).val();
        if (inputValue == "None") {
            maxVcpus = Number.MAX_SAFE_INTEGER;
        } else {
            maxVcpus = inputValue;
        }

        changeTable();
    });

    $('#minMemory').change(function() {
        //Selected value
        var inputValue = $(this).val();
        if (inputValue == "None") {
            minMemory = 0;
        } else {
            minMemory = inputValue;
        }

        changeTable();
    });

    $('#maxMemory').change(function() {
        //Selected value
        var inputValue = $(this).val();
        if (inputValue == "None") {
            maxMemory = 0;
        } else {
            maxMemory = inputValue;
        }

        changeTable();
    });

    $('#iops-value').click(function() {
        iops = this.checked;
        changeTable();
    });

    $('#piops-value').click(function() {
        piops = this.checked;
        changeTable();
    });

    $('#minThreads').change(function() {
        //Selected value
        var inputValue = $(this).val();
        if (inputValue == "None") {
            minThreads = 0;
        } else {
            minThreads = inputValue;
        }

        changeTable();
    });

    $('#maxThreads').change(function() {
        //Selected value
        var inputValue = $(this).val();
        if (inputValue == "None") {
            maxThreads = Number.MAX_SAFE_INTEGER;
        } else {
            maxThreads = inputValue;
        }

        changeTable();
    });

    $('#minCores').change(function() {
        //Selected value
        var inputValue = $(this).val();
        if (inputValue == "None") {
            minCores = 0;
        } else {
            minCores = inputValue;
        }

        changeTable();
    });

    $('#maxCores').change(function() {
        //Selected value
        var inputValue = $(this).val();
        if (inputValue == "None") {
            maxCores = Number.MAX_SAFE_INTEGER;
        } else {
            maxCores = inputValue;
        }

        changeTable();
    });

    $('#search-value').submit(function(e) {
        e.preventDefault();
        if ($("input:first").val()) {
            searchValue = $("input:first").val();
        } else {
            searchValue = '';
        }

        changeTable();
    });

});

var selected = [];

function showFilters() {
    document.getElementById("makingthiseasier").style.visibility = "visible";
    document.getElementById("makingthiseasier").style.left = "0px";
    document.getElementById("aws-nav-flyout-1-rooty").style.visibility =
        "visible";
    document.getElementById("position-me").style.left = "45%";
}

function unshowFilters() {
    unshowSecondFilters();
    document.getElementById("makingthiseasier").style.visibility = "hidden";
    document.getElementById("makingthiseasier").style.left = "-281px";
    document.getElementById("aws-nav-flyout-1-rooty").style.visibility = "hidden";
    document.getElementById("position-me").style.left = "25%";
}

function unshowSecondFilters() {
    var isHovered = !!$('.aws-nav-flyout').filter(function() { return
        $(this).is(":hover"); }).length;

    document.getElementById("makingthiseasier2").style.visibility = "hidden";
    document.getElementById("makingthiseasier2").style.left = "-281px";
    document.getElementById("aws-nav-flyout-2-vcpus").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-memory").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-features").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-threads").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-core").style.visibility =
        "hidden";
}

function showVcpus() {
    document.getElementById("makingthiseasier").style.visibility = "visible";
    document.getElementById("makingthiseasier").style.left = "0px";
    document.getElementById("aws-nav-flyout-1-rooty").style.visibility =
        "visible";
    document.getElementById("makingthiseasier2").style.visibility = "visible";
    document.getElementById("makingthiseasier2").style.left = "281px";
    document.getElementById("aws-nav-flyout-2-vcpus").style.visibility =
        "visible";
    document.getElementById("aws-nav-flyout-2-memory").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-features").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-threads").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-core").style.visibility =
        "hidden";
}

function showMemory() {
    document.getElementById("makingthiseasier").style.visibility = "visible";
    document.getElementById("makingthiseasier").style.left = "0px";
    document.getElementById("aws-nav-flyout-1-rooty").style.visibility =
        "visible";
    document.getElementById("makingthiseasier2").style.visibility = "visible";
    document.getElementById("makingthiseasier2").style.left = "281px";
    document.getElementById("aws-nav-flyout-2-vcpus").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-memory").style.visibility =
        "visible";
    document.getElementById("aws-nav-flyout-2-features").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-threads").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-core").style.visibility =
        "hidden";
}

function showFeatures() {
    document.getElementById("makingthiseasier").style.visibility = "visible";
    document.getElementById("makingthiseasier").style.left = "0px";
    document.getElementById("aws-nav-flyout-1-rooty").style.visibility =
        "visible";
    document.getElementById("makingthiseasier2").style.visibility = "visible";
    document.getElementById("makingthiseasier2").style.left = "281px";
    document.getElementById("aws-nav-flyout-2-vcpus").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-memory").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-features").style.visibility =
        "visible";
    document.getElementById("aws-nav-flyout-2-threads").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-core").style.visibility =
        "hidden";
}

function showThreads() {
    document.getElementById("makingthiseasier").style.visibility = "visible";
    document.getElementById("makingthiseasier").style.left = "0px";
    document.getElementById("aws-nav-flyout-1-rooty").style.visibility =
        "visible";
    document.getElementById("makingthiseasier2").style.visibility = "visible";
    document.getElementById("makingthiseasier2").style.left = "281px";
    document.getElementById("aws-nav-flyout-2-vcpus").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-memory").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-features").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-threads").style.visibility =
        "visible";
    document.getElementById("aws-nav-flyout-2-core").style.visibility =
        "hidden";
}

function showCores() {
    document.getElementById("makingthiseasier").style.visibility = "visible";
    document.getElementById("makingthiseasier").style.left = "0px";
    document.getElementById("aws-nav-flyout-1-rooty").style.visibility =
        "visible";
    document.getElementById("makingthiseasier2").style.visibility = "visible";
    document.getElementById("makingthiseasier2").style.left = "281px";
    document.getElementById("aws-nav-flyout-2-vcpus").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-memory").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-features").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-threads").style.visibility =
        "hidden";
    document.getElementById("aws-nav-flyout-2-core").style.visibility =
        "visible";
}

function toggleFilters() {
    document.getElementById("m-nav-mobile-links").style.visibility = "visible";
}

function select(instance) {
    if (selected.includes(instance)) {
        var index = selected.indexOf(instance);
        selected.splice(index, 1);
    } else {
        selected.push(instance);
    }
    return selected;
}

function get_selected() {
    return selected;
}

function flask_select(myselect) {
    url_string = "select/" + myselect;
    $.ajax({
        type: "GET",
        url: url_string,
        success: function (data) {
            return;
        }
    });
}
