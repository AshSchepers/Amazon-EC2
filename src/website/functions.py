from boto3.dynamodb.conditions import Key, Attr
import sys

import app
dynamo = app.get_dynamo()

def get_prices():
    prices = dynamo.tables['instances'].scan(
        ProjectionExpression='pricing'
    )
    prices = prices['Items']
    uni = []
    for p in prices:
        if p['pricing'] != 'null':
            for key in p['pricing']:
                uni.append(key)
    return list(set(uni))

def get_unique(l):
    l = set(l)
    l = list(l)
    l.sort()
    return l

def get_vals_of(getter):
    vals = []
    response = dynamo.tables['instances'].scan()

    for val in response['Items']:
        try:
            vals.append(int(val[getter]))
        except:
            pass

    vals = get_unique(vals)
    return vals


def get_vals_of_list(getter):
    vals = []
    response = dynamo.tables['instances'].scan()
    for val in response['Items']:
        for v in val[getter]:
            vals.append(v)

    vals = get_unique(vals)
    return vals

def get_vals_of_object(obj, getter):
    vals = []
    response = dynamo.tables['instances'].scan()

    for val in response['Items']:
        lis = val[obj][getter]
        try:
            for l in lis:
                vals.append(int(l))
        except:
            pass

    vals = get_unique(vals)
    return vals

def get_instances():
    response = dynamo.tables['instances'].scan()
    sorted_response = sorted(response['Items'], key=lambda k: k['name'])
    return sorted_response

def get_instance(name):
    response = dynamo.tables['instances'].get_item(
        Key = {'name': name}
    )
    return response

def update_instance(key, setters):
    response = dynamo.tables['instances'].get_item(
        Key = {'name': key}
    )
    pricing = response['Item']['pricing']
    setters['pricing'] = pricing

    print('--------------')
    print(setters)
    print('--------------')
    print(response['Item'])

    dynamo.tables['instances'].put_item(
        Item = setters
    )

def insert_instance(items):
    pricing = "null"
    items["pricing"] = pricing

    try:
        dynamo.tables['instances'].put_item(
                Item = items
        )
        return True
    except:
        return False

def delete_instance(key):
    dynamo.tables['instances'].delete_item(
        Key = {'name': key}
    )

