
import boto3
import json
import decimal
import codecs

dynamodb = boto3.resource('dynamodb', region_name='eu-west-2', aws_access_key_id='AKIAJDMFIUQST5GMZPBQ', aws_secret_access_key='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')

table = dynamodb.Table('instances')
prices = dict()

with open('ec2instances-valid-pricing.json') as f:
    instances = json.load(f)
    config = instances['config']
    currency = config['currencies']
    regions = config['regions']

    for region in regions:
        reg = region['region']
        for instance in region['instanceTypes']:
            for val in instance['sizes']:
                ec2 = val['size']
                if ec2 in prices:
                    prices[ec2][reg] = val['valueColumns'][0]['prices']['USD']

                else:
                    prices[ec2] = dict()

    for key in prices.keys():
        response = table.get_item(
                Key={
                    'name': key,
                    })

        if 'Item' in response and 'name' in response['Item']:
            table.update_item(
                    Key={
                        'name': key,
                        },
                    UpdateExpression="set pricing=:r",
                    ExpressionAttributeValues={
                        ":r": prices[key]
                        }
                    )

